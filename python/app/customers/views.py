from django.shortcuts import render
from rest_framework import viewsets, views
from customers.models import Customer
from customers.serializers import CustomerSerializer
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.decorators import action
from rest_framework.response import Response

class CustomerViewSet(viewsets.ModelViewSet):
    """
    CustomerSet API
    """
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    permission_classes = (AllowAny,)
