from django.db import models

# Create your models here.
class Customer(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=100, blank=True, default='')
    contracts_name = models.CharField(max_length=100, blank=True, default='')
    status = models.BooleanField(default=True)

    class Meta:
        ordering = ('created_at', )
    