from django.db import models

# 合同表
class Contract(models.Model):
    companyName = models.CharField(max_length=200, default='') 
    contactManName = models.CharField(max_length=100, default='')
    contactManPhone = models.CharField(max_length=20, default='')
    contactTel = models.CharField(max_length=20, default='') #座机电话
    projectName = models.CharField(max_length=300, default='') #项目名称
    deviceNumber = models.IntegerField(max_length=11, default=0) # 设备号
    orderNumber = models.IntegerField(max_length=11, default=0) #订单数量
    fax = models.CharField(max_length=20, default='') #传真
    deliveryDate = models.DateTimeField() #交货日期
    shipingAddress = models.CharField(max_length=200, default='') #发货地址
    installPlace = models.CharField(max_length=10, default='国内')
    createdAt = models.DateTimeField(auto_now_add=True)
    exportAt = models.DateTimeField()

# 控制柜参数表
class ControllerCupboard(models.Model):
    contract = models.OneToOneField(Contract, related_name='ControllerCupboard', on_delete=models.CASCADE)
    name = models.CharField(max_length=20, default='') #荣耀柜型号
    size = models.CharField(max_length=20, default='') #荣耀控制柜
    function2G = models.BooleanField(default=True) #2G 互联网功能
    color = models.CharField(max_length=10, default='') # 控制柜颜色
    power = models.CharField(max_length=10, default='') # 功率
    electricLimit = models.CharField(max_length=20, default='') # 额定电流
    releaseVoltageOfSpeedLimiter = models.CharField(max_length=20, default='') #限速器释放电压
    lockVoltage = models.CharField(max_length=20, default='') # 抱闸电压
    standardRequirement1 = models.CharField(max_length=500, default='') # 控制柜标准配置1
    standardRequirement2 = models.CharField(max_length=500, default='') # 控制柜标准配置2
    request1 = models.CharField(max_length=400, default='')
    request2 = models.CharField(max_length=400, default='')
    request3 = models.CharField(max_length=400, default='')
    
    

#曳引机参数
class TractorParam(models.Model):
    contract = models.OneToOneField(Contract, related_name='tractorParams', on_delete=models.CASCADE)
    #曳引钢绳主机 S
    mainEnginNameS = models.CharField(max_length=20, default='') #曳引机型号    
    loadWeightS = models.CharField(max_length=20, default='') #载重
    pulleyDiameterS = models.CharField(max_length=20, default='') # 曳引轮直径
    numberOfRopeGrooveS = models.CharField(max_length=20, default='') # 绳槽数
    encoderLineLengthS = models.CharField(max_length=20, default='') # 编码器线长
    fieldVoltageS = models.CharField(max_length=20, default='') # 现场电压
    # 曳引钢带主机* D
    mainEnginNameD = models.CharField(max_length=20, default='') #曳引机名   
    loadWeightD = models.CharField(max_length=20, default='') #载重
    tiSpeedD = models.CharField(max_length=20, default='') #梯速
    partSteelD = models.CharField(max_length=20, default='') #钢带
    daoLunD = models.CharField(max_length=20, default='') #导轮(FXPD100-06.01)*
    shengjiaD = models.CharField(max_length=20, default='') # 绳夹(AXI-DJ-330)*
    mechineCheckD = models.CharField(max_length=20, default='')# 钢带检测装置*
    #强驱主机* Q
    mainEnginNameQ = models.CharField(max_length=20, default='') #曳引机名   
    fieldVoltageQ = models.CharField(max_length=20, default='') # 现场电压
    encoderLineLengthQ = models.CharField(max_length=20, default='') # 编码器线长
    upHeightQ = models.CharField(max_length=20, default='') # 提升高度*
    decriptionQ = models.TextField(default='') # 强驱主机规格描述：
    #操作箱参数
    operationBoxName = models.CharField(max_length=20, default='') #操作箱型号    
    operationBoxType = models.CharField(max_length=20, default='') #操作箱类型  
    loadWeight = models.CharField(max_length=20, default='') #载重
    pulleyDiameter = models.CharField(max_length=20, default='') # 曳引轮直径
    numberOfRopeGroove = models.CharField(max_length=20, default='') # 绳槽数
    encoderLineLength = models.CharField(max_length=20, default='') # 编码器线长
    fieldVoltage = models.CharField(max_length=20, default='') # 现场电压
    otherOperationBoxTypeDecription = models.TextField(default='') #其他操作箱参数
    #外召盒参数
    externalCallBoxParametersName = models.CharField(max_length=20, default='') #外召盒型号 
    displayCardModel = models.CharField(max_length=20, default='') #显示卡型号
    buttonModel = models.CharField(max_length=20, default='') # 按钮型号
    surfaceTreatmentOfPanel = models.CharField(max_length=20, default='') #面板表面处理
    buttonColor = models.CharField(max_length=20, default='') #按钮发光
    ecboxNumber1 = models.IntegerField(max_length=10, default='') #01-外召盒数量
    ecboX_B = models.CharField(max_length=20, default='') #01-底层单钮向上（-B）
    ecboxNumber2 = models.IntegerField(max_length=10, default='') #02-外召盒数量
    ecboX_BS = models.CharField(max_length=20, default='') #02-底层单钮向上带锁（-BS)
    ecboxNumber3 = models.IntegerField(max_length=10, default='') #03-外召盒数量
    ecboX_M = models.CharField(max_length=20, default='') #03-中间层双钮（-M）
    ecboxNumber4 = models.IntegerField(max_length=10, default='') #04-外召盒数量
    ecboX_MS = models.CharField(max_length=20, default='') #04-中间层双钮带锁（-MS）*
    ecboxNumber5 = models.IntegerField(max_length=10, default='') #05-外召盒数量
    ecboX_T = models.CharField(max_length=20, default='') #05-顶层单钮向下（-T）*
    ecboxNumber6 = models.IntegerField(max_length=10, default='') #06-外召盒数量
    ecboX_TS = models.CharField(max_length=20, default='') #06-顶层单钮向下带锁（-TS）*
    #操作箱名牌
    ecboxLogoMakeMethod = models.CharField(max_length=20, default='') #操纵箱铭牌加工工艺*
    limitPersonNumber = models.CharField(max_length=20, default='') #载客数
    limitWeightNumber = models.CharField(max_length=20, default='') #载重数
    #其它人机配件
    isReqiureFireBox = models.BooleanField(default=True) #消防盒
    man_machineNonStandardRequirement1 = models.CharField(max_length=400, default='') #人机非标要求1
    man_machineNonStandardRequirement2 = models.CharField(max_length=400, default='') #人机非标要求2
    man_machineNonStandardRequirement3 = models.CharField(max_length=400, default='') #人机非标要求3
    
#配件参数
class PartParam(models.Model):
    contract = models.OneToOneField(Contract, related_name='partParams', on_delete=models.CASCADE)
    topCheckBoxA = models.CharField(max_length=100, default='') #娇顶检修箱A
    topCheckBoxB = models.CharField(max_length=100, default='') #娇顶检修箱B
    wellSwitch = models.CharField(max_length=100, default='') #井道开关
    interphone = models.CharField(max_length=100, default='') #娇顶对讲子机
    cableClip = models.CharField(max_length=100, default='')  # 随行电缆夹
    operationMachine = models.CharField(max_length=100, default='')  # 操作器
    integratedMachine = models.CharField(max_length=100, default='')  # 地坑一体机
    wellLight = models.CharField(max_length=100, default='')  # 井道照明灯
    electricSwitch = models.CharField(max_length=100, default='')  # 光电开光
    simulatorSwitch = models.CharField(max_length=100, default='') # 模拟器称重开关
    nonStandardRequirement = models.TextField(default='') #非标要求
    
#楼层信息
class FlowParam(models.Model):
    contract = models.OneToOneField(Contract, related_name='flowParams', on_delete=models.CASCADE)
    limitSpeed = models.CharField(max_length=10, default='') # 额定速度
    skipFlow = models.CharField(max_length=10, default='') # 不停靠楼层
    hostingHeight = models.CharField(max_length=10, default='') # 替身高度
    sumFlow = models.CharField(max_length=10, default='') # 总楼层
    isSameSideOpen = models.BooleanField( default=True) # 是否同侧开门
    kengDepth = models.CharField(max_length=10, default='') # 地坑深度
    sumStopFlow = models.CharField(max_length=10, default='') # 总停靠层数
    backOpenSumFlow = models.CharField(max_length=10, default='') # 后开门层站数
    wellWidth = models.CharField(max_length=10, default='') # 井道宽度
    sumDoor = models.CharField(max_length=10, default='') # 总门数
    backOpenFlowName = models.CharField(max_length=10, default='') # 后开门层名称
    wellDepth = models.CharField(max_length=10, default='') # 井道深度
    minusF3 = models.CharField(max_length=10, default='') # -f3
    minusF2 = models.CharField(max_length=10, default='') # -f2
    minusF1 = models.CharField(max_length=10, default='') # -f1
    F1 = models.CharField(max_length=10, default='') # f1
    F2 = models.CharField(max_length=10, default='') # f2
    F3 = models.CharField(max_length=10, default='') # f3
    F4 = models.CharField(max_length=10, default='') # f4
    F5 = models.CharField(max_length=10, default='') # f5
    F6 = models.CharField(max_length=10, default='') # f6
    nonStandardRequirement = models.TextField(default='') #非标要求

# 运输包装
class transParam(models.Model):
    contract = models.OneToOneField(Contract, related_name='transParams', on_delete=models.CASCADE)
    packageType =  models.CharField(max_length=10, default='') # 包装方式
    transType = models.CharField(max_length=10, default='') # 运输方式
    other =     models.TextField(default='') #其他


    

# 设备表
class Device(models.Model):
    name = models.CharField(max_length=200, default='')
    slug = models.CharField(max_length=200, unique=True,default='')
    description = models.CharField(max_length=200, default='')


# 设备参数表
class DeviceParams(models.Model):
    name = models.CharField(max_length=50, default='')
    isRequired = models.BooleanField(default=True)
    formType = models.CharField(max_length=20, default='text')
    sortNumber = models.CharField(max_length=3, default=0)
    description = models.CharField(max_length=200, default=0)
    validatedRule = models.TextField(default='') # 验证规则
    device = models.ForeignKey(Device, related_name='params', on_delete=models.CASCADE)
    createdAt = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('sortNumber', 'createdAt',)