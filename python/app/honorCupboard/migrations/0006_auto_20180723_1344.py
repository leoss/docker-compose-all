# Generated by Django 2.0.5 on 2018-07-23 13:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('honorCupboard', '0005_auto_20180718_1355'),
    ]

    operations = [
        migrations.AddField(
            model_name='controllercupboard',
            name='color',
            field=models.CharField(default='', max_length=10),
        ),
        migrations.AddField(
            model_name='controllercupboard',
            name='standardRequirement1',
            field=models.CharField(default='', max_length=500),
        ),
        migrations.AddField(
            model_name='controllercupboard',
            name='standardRequirement2',
            field=models.CharField(default='', max_length=500),
        ),
    ]
