from rest_framework import serializers
import json
from honorCupboard.models import Device, DeviceParams, Contract, ControllerCupboard

class DeviceParamsSerializer(serializers.ModelSerializer):
    validatedRule = serializers.SerializerMethodField('get_validated_rule')

    class Meta:
        model = DeviceParams
        fields = ('id', 'name', 'isRequired', 'formType', 'validatedRule', 'description')

    def get_validated_rule(self, object):
        return json.loads(object.validatedRule)
    

class DeviceSerializer(serializers.ModelSerializer):
    params = DeviceParamsSerializer(many=True, read_only=True)

    class Meta:
        model = Device
        fields = ('id', 'name', 'slug', 'description', 'params')

class ControllerCupboardSerializer(serializers.ModelSerializer):
    class Meta:
        model = ControllerCupboard
        fields = ('id', 'name', 'size', 'function2G', 'power')

class ContractSerializer(serializers.ModelSerializer):
    ControllerCupboard = ControllerCupboardSerializer(many=False, read_only=True)

    class Meta:
        model = Contract
        fields = ('id', 'companyName', 'contactManName', 'contactManPhone', 'contactTel', \
                'projectName','deviceNumber', 'orderNumber', 'fax', 'deliveryDate', 'shipingAddress', 'createdAt', 'exportAt',
                'ControllerCupboard'
                )

 

